import { ApiService } from './../../core/services/api.service';
import { Component, OnInit, isDevMode } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public curencies;
  public from = 0;
  public to = 0;
  public rate = 1;
  public from_label = "";
  public to_label = "";

  constructor(
    private _apiService: ApiService
  ) { }

  ngOnInit() {
    let obs = this._apiService.getAllCurencies();
    obs.subscribe(
      data => {
        this.curencies = data;
        this.from_label = data[0].curency_label;
        this.to_label = data[0].curency_label;
      },
      err => {
        isDevMode && console.log(err);
      }
    )
  }

  changeFrom(val) {
    this.from_label = val;
    this.updateRate();
  }

  changeTo(val) {
    this.to_label = val;
    this.updateRate();
  }

  updateRate() {
    let obs = this._apiService.getRate(this.from_label, this.to_label);
    obs.subscribe(
      data => {
        this.rate = data.rate;
        console.log(data);
      },
      err => {
        isDevMode && console.log(err);
      }
    )
  }

}
