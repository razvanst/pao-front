import { Component, OnInit, isDevMode } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-ron-rates',
  templateUrl: './ron-rates.component.html',
  styleUrls: ['./ron-rates.component.scss']
})
export class RonRatesComponent implements OnInit {

  public RONrates;

  constructor(
    private _apiService: ApiService
  ) { }

  ngOnInit() {
    let obs = this._apiService.getRONrates();
    obs.subscribe(
      data => {
        delete data.RON;
        let arr = Object.entries(data);
        this.RONrates = arr;
      },
      err => {
        isDevMode && console.log(err);
      }
    )
  }

}
