import { Component, OnInit, isDevMode } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  public transactions;

  constructor(
    private _apiService: ApiService
  ) { }

  ngOnInit() {
    let obs1 = this._apiService.getAllTransactions();
    obs1.subscribe(
      data => {
        this.transactions = data;
      },
      err => {
        isDevMode && console.log(err);
      }
    )
  }

  formatDate( date ) {
    return date.split("T")[0];
  }

}
