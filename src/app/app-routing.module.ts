import { RonRatesComponent } from './pages/ron-rates/ron-rates.component';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'transactions', component:TransactionsComponent},
  {path:'ron-rates', component:RonRatesComponent},
  {path:'**', component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
