import { apiConfig } from './../../../assets/config/apiConfig';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _apiUrl = apiConfig.baseUrl;

  constructor(
    private http: HttpClient
  ) { }

  getAllTransactions() {
    let obs = this.http.get<any>(this._apiUrl + "/getAllTransactions/DESC");
    return obs;
  }

  getAllCurencies() {
    let obs = this.http.get<any>(this._apiUrl + "/getCurencies");
    return obs;
  }

  getRONrates() {
    let obs = this.http.get<any>(this._apiUrl + "/getRONrates");
    return obs;
  }

  getRate(c1, c2) {
    let obs = this.http.get<any>(this._apiUrl + "/getRatesFor/"+c1+"/"+c2);
    return obs;
  }
}
